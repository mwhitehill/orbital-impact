package gui;

import backend.BackEngine;
import processing.core.PApplet;
import processing.core.PShape;
import processing.core.PShapeOBJ;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by michael on 1/31/17.
 */
public class OrbitalImpact extends PApplet implements EventHandler {
    private BackEngine BE;
    private GUI3Control GC;
    private int width=1280;
    private int height=720;

    private int gridxCount=11;
    private int gridYCount=11;

    private double gridWidth;
    private double gridHeight;


    public OrbitalImpact(){
        BackEngine BE=new BackEngine(this);
        gridWidth=width/gridxCount;
        gridHeight=height/gridYCount;
        GC =new GUI3Control(this);
    }

    public static void main (String [] args){
        PApplet.main("gui.OrbitalImpact");
    }

    private PShape s;

    public void settings(){
        this.size(width,height,P3D);
        BufferedReader buttonFile= null;
        try {
            buttonFile = new BufferedReader(new FileReader("data/button.obj"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        s=new PShapeOBJ(this,buttonFile);
        //System.out.println(s);
        //s.scale(25);

    }

    public void draw(){
        GC.draw();
    }

    public int getWidth(){
        return width;
    }
    public int getHeight(){
        return height;
    }

    public int getGridxCount(){
        return gridxCount;
    }
    public int getGridYCount(){
        return gridYCount;
    }

    public double getGridWidth(){
        return gridWidth;
    }
    public double getGridHeight(){
        return gridHeight;
    }

    public GUI3Control getGUIControl(){
        return GC;
    }


    @Override
    public void event(GUI3Element element) {

    }
}
