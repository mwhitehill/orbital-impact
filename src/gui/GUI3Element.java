package gui;

/**
 * Created by michael on 1/31/17.
 */
abstract public class GUI3Element {
    protected GUI3Control GC;
    protected EventHandler handler;
    protected double x, y, width, height;
    protected double z;

    public GUI3Element (GUI3Control GC, double x, double y, double width, double height, EventHandler handler){
        this.GC=GC;
        this.handler=handler;
        this.x=x; this.y=y; this.width=width; this.height=height;
        this.z=0;
    }
    public GUI3Element (GUI3Control GC, double x, double y, double width, double height, double z, EventHandler handler){
        this.GC=GC;
        this.handler=handler;
        this.x=x; this.y=y; this.width=width; this.height=height;
        this.z=z;
    }

    abstract public void draw();

    public void event(){
        if(handler!=null)
            handler.event(this);
        else
            System.err.println("Clicked unbound element");
    }

    public boolean isClicked(){
        return  (GC.getApplet().mousePressed && !GC.isClickedLastFrame() && GC.getApplet().mouseX > getLeftPixel() && GC.getApplet().mouseX < getRightPixel()
                && GC.getApplet().mouseY > getTopPixel() && GC.getApplet().mouseY < getBottomPixel());
    }


    public float getCenterXPixel(){
        double totalWidth= width*GC.getApplet().getGridWidth();
        double midpoint=totalWidth/2;
        return (float) (x*GC.getApplet().getGridWidth()+midpoint);
    }
    public float getLeftPixel(){
        return (float) (x*GC.getApplet().getGridWidth());
    }
    public float getRightPixel(){
        return (float)  ((x+width)*GC.getApplet().getGridWidth());
    }

    public float getTopPixel(){
        return (float) (y*GC.getApplet().getGridHeight());
    }
    public float getBottomPixel(){
        return (float)  ((y+height)*GC.getApplet().getGridHeight());
    }
    public float getCenterYPixel(){
        double totalHeight= height*GC.getApplet().getGridHeight();
        double midpoint=totalHeight/2;
        return (float) (y*GC.getApplet().getGridHeight()+midpoint);
    }



}
