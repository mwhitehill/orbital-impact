package gui;

/**
 * Created by michael on 2/1/17.
 */
public class MainPanelHandler implements EventHandler {
    private GUI3Control GC;

    //GUI Elements
    GUI3Label nextTurnLabel;
    GUI3Button nextTurn;

    GUI3TextArea shipInfo;
    GUI3TextArea planetInfo;

    public MainPanelHandler(GUI3Control GC){
        this.GC=GC;
        //GC.add(testButton);
        //GC.add(testText);

        nextTurnLabel=new GUI3Label(GC, 1,.2,.5,.5, "Next Turn");
        nextTurn=new GUI3Button(GC,1,0.5,.5,.5,this);

        GUI3Label shipInfoLabel= new GUI3Label(GC,2,.7,1,.4,"Ship Info");
        shipInfo=new GUI3TextArea(GC,1,1,3,6);
        GUI3Label plenetInfoLabel= new GUI3Label(GC,5,.7,1,.4,"Planet Info");
        planetInfo=new GUI3TextArea(GC,4,1,3,6);

        shipInfo.addString("Message 1. that's really long and testing the alignment.");
        shipInfo.addString("Message 2 with\nnew line");

        GC.add(nextTurn);
        GC.add(nextTurnLabel);
        GC.add(shipInfoLabel);
        GC.add(shipInfo);
        GC.add(plenetInfoLabel);
        GC.add(planetInfo);

        // test
        GC.add(new ModeledButton(GC, 8, 3, 1, 1, this));

    }

    @Override
    public void event(GUI3Element element) {
        System.out.println("Hello From Main Panel");
        if(element.equals(nextTurn)){
            nextTurn.press();
            //nextTurn.press();
        }
        // TODO: 2/1/17
    }
}
