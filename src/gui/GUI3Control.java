package gui;

import saito.objloader.OBJModel;

import java.util.ArrayList;

/**
 * Created by michael on 1/31/17.
 */
public class GUI3Control {
    private static OrbitalImpact applet;
    private MainPanelHandler MP;
    private ArrayList<GUI3Element> elements;

    public static final double defaultDepth=5;

    private boolean clickedLastFrame;

    public GUI3Control(OrbitalImpact applet){
        this.applet=applet;
        elements=new ArrayList<>();
        MP=new MainPanelHandler(this);
    }

    public void setup(){

    }

    public void draw(){
        applet.background(127,127,127);
        boolean displayGrids=true;

        if(displayGrids){
            applet.stroke(155);
            for(int i=0; i<applet.getGridxCount(); i++){
                applet.line(i*(float)applet.getGridWidth(), 0, i*(float)applet.getGridWidth(), applet.getHeight());
            }
            for(int i=0; i<applet.getGridYCount(); i++){
                applet.line(0,i*(float)applet.getGridHeight(),applet.getWidth(), i*(float)applet.getGridHeight());
            }
        }
        for(int i=0; i<elements.size(); i++){
            elements.get(i).draw();
        }
        clickedLastFrame=applet.mousePressed;
    }


    public OrbitalImpact getApplet(){
        return applet;
    }

    public void add(GUI3Element e){
        elements.add(e);
    }

    public boolean isClickedLastFrame(){return clickedLastFrame;}

}
