package gui;

import processing.core.PConstants;

/**
 * Created by michael on 1/31/17.
 */
public class GUI3Label extends GUI3Element {
    private String text;

    /**
     *
     * @param GC GU3 Controle holding it
     * @param x Grid X coordinate
     * @param y Grid Y coordinate
     * @param width Grids Spanned Horizontally
     * @param height Grids Spanned Vertically
     * @param text Text
     */
    public GUI3Label(GUI3Control GC,double x, double y, double width, double height, String text) {
        super(GC, x, y, width, height,null);
        this.text=text;
    }

    @Override
    public void draw() {
        GC.getApplet().fill(0);
        GC.getApplet().textAlign(PConstants.CENTER);
        GC.getApplet().text(text, this.getCenterXPixel(),this.getCenterYPixel());
    }

}
