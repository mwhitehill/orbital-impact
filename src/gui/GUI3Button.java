package gui;

import processing.core.PConstants;

/**
 * Created by michael on 1/31/17.
 */
public class GUI3Button extends GUI3Element {
    private int state=0; //0 is unset   1 is off    2 is in transition  3 is onn

    public GUI3Button(GUI3Control GC, double x, double y, double width, double height, EventHandler handler) {
        super(GC, x, y, width, height,GUI3Control.defaultDepth,handler);
    }

    public GUI3Button(GUI3Control GC, double x, double y, double width, double height, double z, EventHandler handler) {
        super(GC, x, y, width, height, z,handler);
    }


    @Override
    public void draw() {
        if (GC.getApplet().mousePressed && !GC.isClickedLastFrame() && GC.getApplet().mouseX > getLeftPixel() && GC.getApplet().mouseX < getRightPixel()
                && GC.getApplet().mouseY > getTopPixel() && GC.getApplet().mouseY < getBottomPixel()) {
            event();
        }

        float x=getLeftPixel();
        float y=getTopPixel();

        //draw rectangle
        GC.getApplet().g.fill(0);
        //forward face
        GC.getApplet().beginShape(PConstants.QUADS);
        GC.getApplet().vertex(getLeftPixel(), getTopPixel(),(float)z);//left top
        GC.getApplet().vertex(getRightPixel(), getTopPixel(),(float)z);//right top
        GC.getApplet().vertex(getRightPixel(), getBottomPixel(),(float)z);//right bottom
        GC.getApplet().vertex(getLeftPixel(), getBottomPixel(),(float)z);//left bottom
        //GC.getApplet().endShape();

        //back face
        //GC.getApplet().beginShape();
        GC.getApplet().vertex(getLeftPixel(), getTopPixel(),0);//left top
        GC.getApplet().vertex(getRightPixel(), getTopPixel(),0);//right top
        GC.getApplet().vertex(getRightPixel(), getBottomPixel(),0);//right bottom
        GC.getApplet().vertex(getLeftPixel(), getBottomPixel(),0);//left bottom
        //GC.getApplet().endShape();

        //right face
        //GC.getApplet().beginShape();
        GC.getApplet().vertex(getRightPixel(),getTopPixel(),(float)z);
        GC.getApplet().vertex(getRightPixel(),getTopPixel(),0);
        GC.getApplet().vertex(getRightPixel(),getBottomPixel(),0);
        GC.getApplet().vertex(getRightPixel(),getBottomPixel(),(float)z);
        //GC.getApplet().endShape();

        //left face
        //GC.getApplet().beginShape();
        GC.getApplet().vertex(getLeftPixel(),getTopPixel(),(float)z);
        GC.getApplet().vertex(getLeftPixel(),getTopPixel(),0);
        GC.getApplet().vertex(getLeftPixel(),getBottomPixel(),0);
        GC.getApplet().vertex(getLeftPixel(),getBottomPixel(),(float)z);
        //GC.getApplet().endShape();

        //top face
        //GC.getApplet().beginShape();
        GC.getApplet().vertex(getLeftPixel(),getTopPixel(),(float)z);
        GC.getApplet().vertex(getRightPixel(),getTopPixel(),(float)z);
        GC.getApplet().vertex(getRightPixel(),getTopPixel(),0);
        GC.getApplet().vertex(getLeftPixel(),getTopPixel(),0);
        //GC.getApplet().endShape();

        //bottom face
        //GC.getApplet().beginShape();
        GC.getApplet().vertex(getLeftPixel(),getBottomPixel(),(float)z);
        GC.getApplet().vertex(getRightPixel(),getBottomPixel(),(float)z);
        GC.getApplet().vertex(getRightPixel(),getBottomPixel(),0);
        GC.getApplet().vertex(getLeftPixel(),getBottomPixel(),0);
        GC.getApplet().endShape();




    }

    public void press(){
        if(z==0){
            z=GUI3Control.defaultDepth;
        }
        else z=0;
        //draw();
    }

}
