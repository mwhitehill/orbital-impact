package gui;

import processing.core.PShape;
import processing.core.PShapeOBJ;
import saito.objloader.OBJModel;

/**
 * Created by dawsoncanby on 2/4/17.
 */
public class ModeledButton extends GUI3Button {

    public ModeledButton(GUI3Control GC, double x, double y, double width, double height, EventHandler handler) {
        super(GC, x, y, width, height, handler);

    }

    public ModeledButton(GUI3Control GC, double x, double y, double width, double height, double z, EventHandler handler) {
        super(GC, x, y, width, height, z, handler);
    }

    @Override
    public void draw() {
        
    }
}
