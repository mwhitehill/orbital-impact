package gui;

import processing.core.PConstants;

import java.util.ArrayList;

/**
 * Created by michael on 2/1/17.
 */
public class GUI3TextArea extends GUI3Element {
    private ArrayList<message> messages;

    public GUI3TextArea(GUI3Control GC, double x, double y, double width, double height) {
        super(GC, x, y, width, height,null);
        messages=new ArrayList<>();
    }

    public GUI3TextArea(GUI3Control GC, double x, double y, double width, double height, double z) {
        super(GC, x, y, width, height, z,null);
        messages=new ArrayList<>();
    }

    public GUI3TextArea(GUI3Control GC, double x, double y, double width, double height, EventHandler handler) {
        super(GC, x, y, width, height,handler);
        messages=new ArrayList<>();
    }

    public GUI3TextArea(GUI3Control GC, double x, double y, double width, double height, double z, EventHandler handler) {
        super(GC, x, y, width, height, z,handler);
        messages=new ArrayList<>();
    }

    public void addString(String s){
        messages.add(new message(s));
    }
    public void removeString(String s){
        for(int i=0; i<messages.size(); i++){
            if(messages.get(i).equals(s)){
                messages.remove(i);
            }
        }
    }

    private Triplet getColor(int linCount){
        if(linCount%5==0){
            return new Triplet(209,2,2);
        }
        if(linCount%4==0){
            return new Triplet(36,209,2);
        }
        if(linCount%3==0){
            return new Triplet(219,239,0);
        }
        else
            return new Triplet(4,0,147);
    }

    @Override
    public void draw() {
        //draw box
        GC.getApplet().fill(0);
        GC.getApplet().rectMode(PConstants.CORNERS);
        GC.getApplet().rect(getLeftPixel(),getTopPixel(),getRightPixel(),getBottomPixel());
        GC.getApplet().fill(150);
        GC.getApplet().rect(getLeftPixel()+5, getTopPixel()+5, getRightPixel()-5, getBottomPixel()-5);

        float screenLeft=getLeftPixel()+7;
        float screenRight=getRightPixel()-7;
        float screenTop=getTopPixel()+7;
        float screenBottom=getBottomPixel()-7;

        float lineHeight=16;


        GC.getApplet().fill(0,34,89);

        GC.getApplet().textSize(12);
        //place text inset
//        for(int i=0; i<data.size(); i++){
//            GC.getApplet().text(data.get(i),screenLeft,screenTop+(i*lineHeight), screenRight,screenTop+((i+1)*lineHeight));
//        }
        int lineCount=0;
        for(int m=0; m<messages.size(); m++){
            Triplet color=getColor(m);
            for(int line=0; line<messages.get(m).size(); line++){
                GC.getApplet().fill(color.getV1(),color.getV2(),color.v3);
                GC.getApplet().textAlign(PConstants.LEFT);
                GC.getApplet().text(messages.get(m).get(line),screenLeft,screenTop+(lineCount*lineHeight),screenRight,screenTop+(lineCount+1)*lineHeight);
                lineCount++;
            }
        }
    }

    private class message{
        String[] lines;
        String origin;
        private message(String s){
            origin=s;
            lines=s.split("\n");
        }
        private int size(){
            return lines.length;
        }
        private String get(int i){
            if(i>lines.length)
                throw new ArrayIndexOutOfBoundsException("Tried to access element: "+i+" Size goes to: "+lines.length);
            else
                return lines[i];
        }

        private boolean equals(String s){
            return s.equals(origin);
        }
    }
    private class Triplet{
        private int v1,v2,v3;
        Triplet(int a, int b, int c){
            v1=a;v2=b;v3=c;
        }
        private int getV1(){
            return v1;
        }
        private int getV2(){
            return v2;
        }
        private int getV3(){
            return v3;
        }
    }

}
