package backend;


/**
 * Created by michael on 1/15/17.
 */
public abstract class SubSystem {
    protected String name;
    protected Vessel craft;
    protected boolean onStatus=false;
    protected boolean disabled=false;

    public SubSystem(Vessel craft){
        onStatus=false;
        this.craft=craft;
        name = "NO NAME";
    }

    public boolean getOnStatus(){
        return onStatus;
    }
    public void setOnStatus(boolean onStatus){
        this.onStatus=onStatus;
    }
    abstract public int blow(int incomingDamage);

    abstract public void runTurn();
    public String getName(){return name;}

    /**
     * This only needs to be implimented by subsystems that draw power. This method will be called when a power generator is shutdown without adiquite overhead power
     */
    abstract public void forceShutdown();

}
