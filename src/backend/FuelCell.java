package backend;


/**
 * Created by michael on 1/18/17.
 */
public class FuelCell extends SubSystem {
    private int powerOut = 40;
    private int heatOut = 30;

    private boolean changePower;

    public FuelCell(Vessel craft) {
        super(craft);
        super.name="Basic Fuel Cell";
        onStatus=false;
        changePower=false;
    }

    private int getECout(){
        if(onStatus)
            return powerOut;
        else
            return 0;
    }

    private int getHeatOut(){
        if(onStatus)
            return powerOut;
        else
            return 0;
    }

    public String toString(){
        String s="Power Out: "+getECout()+"\n"+
                "Heat Out: "+getHeatOut();
        return s;
    }

    @Override
    public int blow(int incomingDamage) {
        return 0;
    }

    @Override
    public void runTurn() {
        if(changePower) {
            changePower = false;
            onStatus = true;
            craft.addPower(getECout());
            craft.heatVessel(getHeatOut());
        }
    }

    @Override
    public void forceShutdown() {
        craft.drawPower(getECout());
        craft.coolVessel(getHeatOut());
        onStatus=false;
    }
}