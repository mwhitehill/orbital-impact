package backend;


import java.util.ArrayList;

/**
 * Created by michael on 1/21/17.
 */



public class MassCannon extends SubSystem {
    private int propellentRange=100;

    private boolean changeOnStatus;
    private boolean armed;
    private boolean hasFired;

    private int idleECDraw=5;
    int fireREadyECDraw=15;

    int idleHeat=5;
    int postFireHeatOut=15;


    private Vessel target;

    public MassCannon(Vessel craft) {
        super(craft);
        name = "Basic Mass Cannon";
        changeOnStatus=false;
        armed=false;
        hasFired=false;
    }

    private int getCurrentECDraw(){
        if(!onStatus)
            return 0;
        if(armed)
            return fireREadyECDraw;
        else
            return idleHeat;
    }

    private int getCurrentHeatOut(){
        if(!onStatus)
            return 0;
        if(hasFired)
            return postFireHeatOut;
        else
            return idleHeat;
    }

    private String getShotData(){
        if(target==null)
            return "No Shot Data";
        else{
            String s="Target: "+target.getName()+"\n"+
                    "Projectile Mass: "+calculateProjectileMass(target)+"\n"+
                    "Fire Ready: "+armed;
            return s;
        }
    }

    public String toString(){
        String s="EC Draw: "+getCurrentECDraw()+"\n"+
                "Heat Out+ "+getCurrentHeatOut();
        return s;
    }

    @Override
    public int blow(int incomingDamage) {
        return 0;
    }

    @Override
    public void runTurn() {

    }

    @Override
    public void forceShutdown() {

    }

    private void togglePowerEvent(){

    }

    private int calculateProjectileMass(Vessel target){
        int shotRange=craft.getCurrentOrbit().distanceBetween(target.getCurrentOrbit());
        int shotMass=propellentRange-shotRange;
        return shotMass;
    }

}
