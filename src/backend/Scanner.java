package backend;


import java.util.ArrayList;

/**
 * Created by michael on 1/19/17.
 */
public class Scanner extends SubSystem{
    private boolean changeOnStatus;
    private int detectionVal=45;

    private int ecDraw=10;
    private int heatOut=10;

    private ArrayList<Vessel> discovoredVessels;

    public Scanner(Vessel craft) {
        super(craft);
        super.name="Basic Scanner";
        discovoredVessels=new ArrayList<>();
    }

    public String toString(){
        String s="EC Draw"+getCurrentECDraw()+"\n"+
                "Heat Out: "+getCurrentHeatOut()+"\n"+
                "Scan Strength: "+detectionVal;
        return s;
    }

    private int getCurrentECDraw(){
        if(onStatus)
            return ecDraw;
        else
            return 0;
    }

    private int getCurrentHeatOut(){
        if(onStatus)
            return heatOut;
        else
            return 0;
    }

    private String listDiscovoredVessels(){
        String s="";
        for(int i=0; i<discovoredVessels.size(); i++){
            s+=discovoredVessels.get(i).getName()+"\n";
        }
        return s;
    }

    @Override
    public int blow(int incomingDamage) {
        return 0;
    }


    // TODO: 1/20/17 Impliment logic for the scan itself 
    @Override
    public void runTurn() {
        if(onStatus){
            for(int i=0; i<discovoredVessels.size(); i++){
                craft.removeKnownVessel(discovoredVessels.get(i));
            }
            discovoredVessels.clear();
            for(int bodies=0; bodies<craft.getBE().getSolSystem().getSize(); bodies++){
                for(int orbiters=0; orbiters<craft.getBE().getSolSystem().getBody(bodies).getOrbitingVessels().size(); orbiters++){
                    if(craft.getBE().getSolSystem().getBody(bodies).getOrbitingVessels().get(orbiters).getHeatOut()>detectionVal){
                        discovoredVessels.add(craft.getBE().getSolSystem().getBody(bodies).getOrbitingVessels().get(orbiters));
                    }
                }
            }
            for(int i=0; i<discovoredVessels.size(); i++){
                craft.addKnownVesel(discovoredVessels.get(i));
            }
        }

        if(changeOnStatus==true){
            onStatus=true;
            if(craft.drawPower(getCurrentECDraw()))
                craft.heatVessel(getCurrentHeatOut());
            else
                onStatus=false;
        }
        changeOnStatus=false;

        if(onStatus==false)
            discovoredVessels.clear();
    }

    @Override
    public void forceShutdown() {
        craft.coolVessel(getCurrentHeatOut());
        craft.addPower(getCurrentECDraw());
        discovoredVessels.clear();
        onStatus=false;
    }


}
