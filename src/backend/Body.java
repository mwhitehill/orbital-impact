package backend;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by michael on 12/23/16.
 * The backend.Body class models all celestial bodies that can be orbited.
 * As per real life the fuel costs to transfer from one body to another change throughout their orbit, this is modeled in the variables pertaining to variance, and orbital velocity.
 *
 */
public class Body {
    //used for Equals method
    private String name;
    private SolSystem system;

    //an arraylist used to keep track of the orbiting vessels.
    private ArrayList<Vessel> orbitingVessels=new ArrayList<>();

    //linked lists of planets, used for calculating dV costs
    private Body inferior;
    private Body posterior;

    //the cost of transfer from inferior or posterior
    public int inferiorDistance;
    public int posteriorDistance;

    //used to represent transfer windows, increases/decreases transfer cost
    private int originalDistance;
    private int distanceVariance;
    private int activeVariance;
    private boolean increasingVariance;
        //the number of turns it takes the transfer cost to cycle
    private int orbitalVelocity;

    //Determines planet charecteristics
    private int tier;

    //the planets index (0 is closest to sun)
    private int rank;

    /**
     *
     * @param system Solar System containing planet
     * @param planetTier Loosely determines planet behavior
     * @param rank The index within the solar system
     * @param name The name of the planet
     */
    public Body(SolSystem system, int planetTier, int rank, String name){
        this.name=name;
        this.rank=rank;
        this.system=system;
        this.tier=planetTier;
        Random rand=new Random();
        if(planetTier==1)
            createTier1(rand);
        if(planetTier==2)
            createTier2(rand);
        if(planetTier==3)
            createTier3(rand);


        if(rank!=0) {
            inferior = system.getBody(rank - 1);
            system.getBody(rank-1).setPosterior(this);
            inferior.setPosteriorDistance(inferiorDistance);
        }

    }

    private void createTier1(Random rand){
        //handling orbital variance
        distanceVariance=rand.nextInt(3)+1;
        activeVariance=0;
        increasingVariance=rand.nextBoolean();

        orbitalVelocity=rand.nextInt(2);

        //handling orbital values
        if(rank==0)
            inferiorDistance=0;
        else {
            inferiorDistance=rand.nextInt(5)+13;
        }
        originalDistance=inferiorDistance;
    }

    private void createTier2(Random rand){
        //handling orbital variance
        distanceVariance=rand.nextInt(4)+5;
        activeVariance=0;
        increasingVariance=rand.nextBoolean();

        orbitalVelocity=rand.nextInt(5)+6;

        //handling orbital values
        if(rank==0)
            inferiorDistance=0;
        else {
            inferiorDistance=rand.nextInt(20)+31;
        }
        originalDistance=inferiorDistance;
    }

    private void createTier3(Random rand){
        //handling orbital variance
        distanceVariance=rand.nextInt(8)+7;
        activeVariance=0;
        increasingVariance=rand.nextBoolean();

        orbitalVelocity=rand.nextInt(6)+11;

        //handling orbital values
        if(rank==0)
            inferiorDistance=0;
        else {
            inferiorDistance=rand.nextInt(30)+61;
        }
        originalDistance=inferiorDistance;
    }

    public String toString(){
        String s="BODY "+name+"{ Rank: "+rank+" Inferior Distance: "+inferiorDistance+" Posterior Distance: "+posteriorDistance;
        s+=" Orbital Variance: "+distanceVariance+" Increasing Variance: "+increasingVariance+" Active Variance: "+activeVariance+" Number of Orbiting Bodies: "+orbitingVessels.size()+"}";
        return s;
    }

    /**
     * get the GUI data to display
     * @param origin the body where the ship is located
     * @return gui data
     */
    public String getGUIData(Body origin) {
        return "Name: "+name+"\n"+
                "DV From Current: "+distanceBetween(origin)+"\n"+
                "Rank: "+this.rank+"\n"+
                "Body Tier: "+this.tier+"\n"+
                "==--Raw Orbital Data--=="+"\n"+
                "Velocity: "+this.orbitalVelocity+"\n"+
                "Variance: "+this.distanceVariance+"\n"+
                "Active Variance: "+this.activeVariance+"\n"+
                "Origin Distance: "+this.originalDistance+"\n"+
                "Increasing: "+this.increasingVariance;
    }

    /**
     *
     * @param origin The backend.Body that the current vessel is orbiting
     * @return JPanel holding planet information for the GUI
     */
    public JPanel getLargeDisplay(Body origin){
        String rawData=
                "Name: "+name+"\n"+
                        "DV From Current: "+distanceBetween(origin)+"\n"+
                        "Rank: "+this.rank+"\n"+
                        "backend.Body Tier: "+this.tier+"\n"+
                        "==--Raw Orbital Data--=="+"\n"+
                        "Velocity: "+this.orbitalVelocity+"\n"+
                        "Variance: "+this.distanceVariance+"\n"+
                        "Active Variance: "+this.activeVariance+"\n"+
                        "Origin Distance: "+this.originalDistance+"\n"+
                        "Increasing: "+this.increasingVariance;
        JTextArea rawOutputDisplay=new JTextArea(rawData);
        JPanel root=new JPanel();
        root.add(rawOutputDisplay);
        return root;
    }

    /**
     *
     * @param target The destination planet.
     * @return The distance between current and the target planet
     */
    public int distanceBetween(Body target){
        int totalCost=0;
        if(target.equals(this))
            return totalCost;

        Body traverser=this;
        if(target.getRank()>this.rank){
            while (!traverser.equals(target)){
                totalCost=totalCost+traverser.getPosteriorDistance();
                traverser=traverser.posterior;
            }
        }
        if(target.getRank()<this.rank){
            while (!traverser.equals(target)){
                totalCost=totalCost+traverser.getInferiorDistance();
                traverser=traverser.inferior;
            }
        }
        return totalCost;
    }

    /**
     * Puts a vessel object int the list of currently orbiting vessels
     * @param craft
     */
    public void insertOrbit(Vessel craft){
        orbitingVessels.add(craft);
    }

    /**
     * removes a vessel from the list of currently orbiting items
     * @param craft
     */
    public void removeVessel(Vessel craft){
        orbitingVessels.remove(craft);
    }

    /**
     * runs the turn for a planet, handling changes in orbital position and redoes dV cost for neighbours.
     */
    public void runTurn(){
        //calculate and apply variance
        //incriment active variance

        //apply variance
        if(rank!=0){
            inferiorDistance=originalDistance+activeVariance;

            if(activeVariance==distanceVariance){
                increasingVariance=!increasingVariance;
            }
            if(activeVariance==(-1*distanceVariance))
                increasingVariance=!increasingVariance;
            if(increasingVariance==true)
                activeVariance++;
            else
                activeVariance--;
            inferior.setPosteriorDistance(inferiorDistance);
        }

    }

    public boolean equals(Object other){
        if(!(other instanceof Body)){
            return false;
        }
        else if(((Body) other).getRank()==this.rank)
            return true;
        else
            return false;
    }

    //getters and setters

    public void setPosteriorDistance(int distance){
        this.posteriorDistance=distance;
    }
    public void setPosterior(Body body){
        posterior=body;
    }

    public String getName() {
        return name;
    }
    public ArrayList<Vessel>getOrbitingVessels(){
        return orbitingVessels;
    }
    public int getRank(){return rank;}
    public int getPosteriorDistance(){return posteriorDistance;}
    public int getInferiorDistance(){return inferiorDistance;}
    public Body getInferior(){return inferior;}
    public Body getPosterior(){return posterior;};


    //Intelij Autogen
    public void setName(String name) {
        this.name = name;
    }

    public void setOrbitingVessels(ArrayList<Vessel> orbitingVessels) {
        this.orbitingVessels = orbitingVessels;
    }

    public void setInferior(Body inferior) {
        this.inferior = inferior;
    }

    public void setInferiorDistance(int inferiorDistance) {
        this.inferiorDistance = inferiorDistance;
    }

    public int getDistanceVariance() {
        return distanceVariance;
    }

    public void setDistanceVariance(int distanceVariance) {
        this.distanceVariance = distanceVariance;
    }

    public int getActiveVariance() {
        return activeVariance;
    }

    public void setActiveVariance(int activeVariance) {
        this.activeVariance = activeVariance;
    }

    public boolean isIncreasingVariance() {
        return increasingVariance;
    }

    public void setIncreasingVariance(boolean increasingVariance) {
        this.increasingVariance = increasingVariance;
    }

    public int getOrbitalVelocity() {
        return orbitalVelocity;
    }

    public void setOrbitalVelocity(int orbitalVelocity) {
        this.orbitalVelocity = orbitalVelocity;
    }

    public int getTier() {
        return tier;
    }

    public void setTier(int tier) {
        this.tier = tier;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}

/*
planet tier is broken into 3 tiers in terms of how far they are from the sun. See Tier behavior in  backend.SolSystem
 */
