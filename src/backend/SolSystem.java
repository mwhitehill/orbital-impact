package backend;

/**
 * Created by michael on 12/23/16.
 * This class models a solar system, keeping track of all it's orbiting bodies and running the turns and interactions between them.
 *
 */
public class SolSystem {
    private String name;
    private Body[] celestials;

    /**
     * Takes the number of planets and creates a solar system with that number of bodies.
     * @param numPlanets
     */
    public SolSystem(int numPlanets){
        //break into three parts
            //close rockyint
            //mid distance rocky+gas
            //long distance gas+rocky
        celestials=new Body[numPlanets];
        celestials[0]=new Body(this,1,0,"Planet 0");
        celestials[1]=new Body(this,1,1,"Planet 1");
        celestials[2]=new Body(this,2,2,"Planet 2");
        celestials[3]=new Body(this,2,3,"Planet 3");
        celestials[4]=new Body(this,3,4,"Planet 4");
        celestials[5]=new Body(this,3,5,"Planet 5");
    }

    /**
     *
     * @param i returns planet at index
     * @return backend.Body at index
     */
    public Body getBody(int i){
        return celestials[i];
    }

    /**
     *
     * @param turnNumber Takes current turn number, updates all Bodies within the solar system.
     */
    public void runTurn(int turnNumber){
        for(int i=0; i<celestials.length; i++){
            celestials[i].runTurn();
        }
        //System.out.println(this);
    }

    public String toString(){
        String s="";
        for(int i=0; i<celestials.length; i++){
            s+=(celestials[i])+"\n";
        }
        return s;
    }

    /**
     *
     * @return number of bodies in solar system
     */
    public int getSize(){return celestials.length;}


}

/*
tier 1 planets are rocky, low orbital velocities, small transfer variance
tier 2 planets are rocky and gas, medium orbital velocities, medium to high variance
tier 3 planets are gas or rocky, high orbital velocities, high variance
 */
