package backend;

import gui.OrbitalImpact;

import java.util.ArrayList;

/**
 * Created by michael on 12/23/16.
 *
 * This class will eventually encapsolate all back end logic, and contain all getters and setters for communication with the front end
 * This class will also be responsible for running turns and checking when the game is over.
 */
public class BackEngine {
    //the turn number of the game
    private int turn;

    //the current solar system
    private SolSystem system;

    //the current vessel, eventually this will be replaced with a player instance
    private Vessel activeVessel;

    private ArrayList<Vessel> vessels;

    OrbitalImpact game;

    public BackEngine(OrbitalImpact game){
        turn=0;
        vessels=new ArrayList<Vessel>();
        this.game=game;
        system = new SolSystem(6);
        activeVessel=new Vessel(system, this,"Asunder");
        vessels.add(activeVessel);
        vessels.add(new Vessel(system, this,"Lusitania"));
        activeVessel=vessels.get(1);
        activeVessel.addSubSystem(new FuelCell(activeVessel));
        activeVessel.addSubSystem(new MassCannon(activeVessel));
        System.out.println(system);
        System.out.println(activeVessel);
    }


    public void initiateGame(){
        //get player name

        //initialize player

        //repeat

        //create solar system

        //insert player into solar system

    }

    public void runTurn(){
        turn=turn+1;
        activeVessel.runTurn();
        system.runTurn(turn);


        //debug statements
        //System.err.println(activeVessel.willJump());
    }

    public SolSystem getSolSystem(){return system;}

    public OrbitalImpact getGame(){return game;}

    public Vessel getAtiveVessel(){return activeVessel;}

    public void setActiveVessel(Vessel v){activeVessel=v;}

    public int getTurn(){
        return turn;
    }

    public Vessel getActiveVessel() {
        return activeVessel;
    }

    public Vessel getVessel(int i){
        return vessels.get(i);
    }
    public ArrayList<Vessel> getVessels(){
        return vessels;
    }
    public void addVessel(Vessel v){
        vessels.add(v);
    }
    public void removeVessel(Vessel v){
        vessels.remove(v);
    }
    public void removeVessel(int i){
        vessels.remove(i);
    }


}
