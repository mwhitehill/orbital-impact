package backend;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by michael on 12/23/16.
 * This class models a vessel, or a spaceship, whatever the fuck you call it.
 * It keeps track of it's location, and usses an inner class, jumpplath to keep track of moving from one planet to the next.
 *
 */
public class Vessel {
    private String name;
    BackEngine BE;

    //the current body the vessel is orbiting
    private Body currentOrbit;
    private SolSystem system;
    //stores the ship destination and time to arrival
    private jumpPath currentJump;
    //The maximum dV the ship can travel in one turn
    //// TODO: 1/15/17 Change the way potential dV is handled: get it from the engine class
    //current electricity being expended
    int availableEC;
    //EC in use
    int usedEC;
    //current heat being emmited
    int heatOut;
    //a list of ship subsystems
    private ArrayList<SubSystem> subSystems;

    //a list of detected/visable vessels
    private ArrayList<Vessel> knownVessels;


    private boolean justJumped=false;

    /**
     * A class modeling the current jump: The target, the dv cost, and the turn cost if necessary.
     */
    private class jumpPath{
        Body target;
        int targetRank;
        int currentRank;
        int dVSum;
        int turnRequirement;
        int calculationdV;
        boolean pathSet;

        private jumpPath(Body target){
            this.target=target;
            targetRank=target.getRank();
            currentRank=currentOrbit.getRank();

            if(currentRank>targetRank)
                createInwardPlan();
            if(currentRank<targetRank)
                createOutwardPlan();
            if(currentOrbit==target){
                dVSum=0;
                turnRequirement=0;
            }
            calculationdV=getPotentialDV();
            pathSet=true;
        }
        private void createInwardPlan(){
            Body currentTarget=currentOrbit;
            while(!currentTarget.equals(target)){
                dVSum=dVSum+currentTarget.getInferiorDistance();
                currentTarget=currentTarget.getInferior();
            }
            if(dVSum<getPotentialDV())
                turnRequirement=0;
            else
                turnRequirement=dVSum/getPotentialDV();
        }

        private void createOutwardPlan(){
            Body currentTarget=currentOrbit;
            while (!currentTarget.equals(target)){
                dVSum=dVSum+currentTarget.getPosteriorDistance();
                currentTarget=currentTarget.getPosterior();
            }
            if(dVSum<getPotentialDV())
                turnRequirement=0;
            else
                turnRequirement=dVSum/getPotentialDV();
        }

        public String toString(){
            String s="Jump Path{Current Location: "+system.getBody(currentRank).getName()+" Target: "+system.getBody(targetRank).getName()+" dV Cost: "+dVSum+" Turn Cost: "+turnRequirement+"}";
            return s;
        }
        private void decrimentTurn(){
            if(turnRequirement==0)
                return;
            turnRequirement--;
        }
        private boolean canJump(){
            return getEffectiveDV()==calculationdV;
        }
    }

    /**
     * crates a new vessel, requires the solar system it's part of
     * @param system
     */
    public Vessel(SolSystem system, BackEngine BE, String name){
        this.BE=BE;
        this.system=system;
        this.name=name;
        subSystems=new ArrayList<>();
        knownVessels=new ArrayList<Vessel>();
        currentOrbit=getSystem().getBody(3);
        currentOrbit.insertOrbit(this);

        usedEC=0;
        availableEC=0;

        subSystems.add(new Engine(this));
        subSystems.add(new FuelCell(this));
        subSystems.add(new backend.Scanner(this));
    }

    public void createFromFile(){
        File vesselFile=new File("vessel.vs");
        try {
            Scanner input=new Scanner(vesselFile);
            name=input.next();
            currentOrbit=system.getBody(input.nextInt());
            //availableDV=0;
            subSystems=new ArrayList<>();
            subSystems.add(new Engine(this));
        } catch (FileNotFoundException e) {
            System.err.println("Failed to read vessel.vs");
            e.printStackTrace();
        }


    }

    /**
     * Runs the turn for the vessel
     */
    public void runTurn() {
        Scanner input=new Scanner(System.in);

        //get other actions (subsystems, scans, attacks, etc)
        //make jump
        justJumped=false;
        if(!(currentJump==null)){
            if(currentJump.turnRequirement==0){
                //System.err.println("Jumping");
                doJump();
                justJumped=true;
            }
        }
        if(currentJump!=null) {
            currentJump.decrimentTurn();
        }
        for (int i=0; i<subSystems.size(); i++){
            subSystems.get(i).runTurn();
        }
    }

    private void doJump(){
        if(currentJump.canJump()){
            currentOrbit.removeVessel(this);
            currentOrbit=currentJump.target;
            currentOrbit.insertOrbit(this);
            currentJump=null;
        }
        else {
            currentJump=null;
            JOptionPane.showMessageDialog(null,"Jump Failed Due To Engine Failure");
        }

    }

    /**
     * Creates a publicjump given a destination
     * @param dest
     */
    public void createJump(Body dest){
        if(getPotentialDV()==0)
            return;
        currentJump=new jumpPath(dest);
    }

    public String toString(){
        String s="VESSEL "+name+"{Current backend.Body: "+currentOrbit.getName()+" Potential dV: "+getPotentialDV()+" Effective dV: "+getEffectiveDV()+" Available EC: "+availableEC+"}";
        return s;
    }

    /**
     * Returns the string to be displayed inside of the JText Area for the GUI for ship information
     * @return
     */
    public String getGUIData(){
        String s= "Current Body: "+currentOrbit.getName()+"\n"+
                "Potential dV: "+getPotentialDV()+"\n"+
                "Effective dV "+getEffectiveDV()+"\n"+
                "Available EC: "+availableEC+"\n"+
                "Heat Out: "+heatOut;
        return s;
    }

    /**
     * Returns a string to be displayed the jump JText Area for the gui
     * @return
     */
    public String getGUIJumpData(){
        if(currentJump==null){
            return "No Jump Planned";
        }
        String s= "Jump Destination: "+currentJump.target.getName()+"\n"+
                "Jump dV: "+currentJump.dVSum+"\n"+
                "Turn Cost: "+currentJump.turnRequirement;
        return s;
    }


    public boolean willJump(){
        if(currentJump==null)
            return false;
        if(currentJump.turnRequirement<=1)
            return true;
        else
            return false;
    }

    public SubSystem getSubsystem(int i){
        return subSystems.get(i);
    }

    // TODO: 1/18/17 Immpliment a Draw Power for when generators are shut down
    public boolean drawPower(int i){
        if(availableEC-i<0)
            return false;
        else
            availableEC-=i;
        return true;
    }

    public void removePower(int p){
        if(availableEC-p>0)
            drawPower(p);
        else {
            int i=0;
            while (i<subSystems.size()&&availableEC-p<0){
                if(subSystems.get(i).getOnStatus())
                    subSystems.get(i).forceShutdown();
                i++;
            }
            if(availableEC-p<0)
                System.err.println("Error in vessel power distribution logic. Negative Power after all subsystems are shutdown.");
            else
                drawPower(p);
        }
    }

    public void addPower(int i){
        if(i<0)
            throw new ArithmeticException("Cannot add negative power");
        else
            availableEC+=i;
    }

    public void heatVessel(int i){
        heatOut+=i;
    }
    public void coolVessel(int i){
        heatOut-=i;
    }


    public int getPotentialDV(){
        int total=0;
        for (int i=0; i<subSystems.size(); i++){
            if(subSystems.get(i) instanceof Engine){
                Engine e = (Engine) subSystems.get(i);
                if(e.isActive)
                    total+=e.getMaxDv();
            }
        }
        return total;
    }

    public int getEffectiveDV(){
        int total=0;
        for(int i=0; i<subSystems.size(); i++){
            if(subSystems.get(i) instanceof Engine){
                Engine e = (Engine) subSystems.get(i);
                total+=e.getEffectivedV();
            }
        }
        return total;
    }

    //Intelij autogen
    public String getName() {
        return name;
    }

    public Body getCurrentOrbit(){return currentOrbit;}

    public void setName(String name) {
        this.name = name;
    }

    public void setCurrentOrbit(Body currentOrbit) {
        this.currentOrbit = currentOrbit;
    }

    public SolSystem getSystem() {
        return system;
    }

    public void setSystem(SolSystem system) {
        this.system = system;
    }

    public jumpPath getCurrentJump() {
        return currentJump;
    }

    public void setCurrentJump(jumpPath currentJump) {
        this.currentJump = currentJump;
    }


    public int getHeatOut() {
        return heatOut;
    }

    public void setHeatOut(int heatOut) {
        this.heatOut = heatOut;
    }

    public ArrayList<SubSystem> getSubSystems() {
        return subSystems;
    }

    public void setSubSystems(ArrayList<SubSystem> subSystems) {
        this.subSystems = subSystems;
    }

    public int getAvailableEC() {
        return availableEC;
    }

    public void setAvailableEC(int availableEC) {
        this.availableEC = availableEC;
    }

    public int getUsedEC() {
        return usedEC;
    }

    public void setUsedEC(int usedEC) {
        this.usedEC = usedEC;
    }

    public boolean isJustJumped() {
        return justJumped;
    }

    public void setJustJumped(boolean justJumped) {
        this.justJumped = justJumped;
    }

    public void removeKnownVessel(Vessel v){
        knownVessels.remove(v);
    }

    public void addKnownVesel(Vessel v){knownVessels.add(v);}

    public ArrayList<Vessel> getKnownVessels(){return knownVessels;}

    public void addSubSystem(SubSystem s){
        subSystems.add(s);
    }

    public BackEngine getBE(){return BE;}

    public void takeRawDamage(int d){


    }

}
