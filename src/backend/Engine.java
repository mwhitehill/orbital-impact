package backend; /**
 * Created by michael on 1/15/17.
 */

/**
 * This class is meant to be the starter tutorial class for beginners writing subsystems. While it is not the most simple subsystem it demonstrates two modes of state changes for a subsystem: instant changes, and turn based changes.
 * An instant change sets values of variables immediately when the button is clicked. For example: shutting down the engine and setting the active/inactive value are instant changes. The moment the button is clicked the changes are applied.
 * A turn based change is one that requires a turn to apply, this includes turning on the engine and warming the engine. When the turn on button is clicked, the value of the button is changed, but the onstatus does not until the end of the turn.
 * When working with turn based changes, an additional boolean variable should be kept to track the changes to be applied when runTurn is called.
 *
 * When creating the GUI elements for a subClass, keep GUI elements as class variables. Avoid recreating elements to change them, instead you should use methods like .setText to update them.
 * Upon construction all GUI elements are fully initialized with invalid data, this way updatePanel can be called to set appropriate data without encountering null pointers.
 * It's recommended that in your subSystem you use an updatePanel method to refresh the data displayed whenever variables may change.
 *
 * For event handling, every time a element in the subsystem panel is clicked onGUIAction is called, passing the element that was clicked/interacted with. Then it's recommended you use .equals to compare it with all elements you've
 * created to determine which was clicked (This is why declaring GUI elements as class variables is recommended).
 *
 * The logic for state changes, things like turning the subsystem on and off, or warming and cooling should be coded carefully to avoid Heat and EC leaks (meaning your subsystem continuously draws or adds power/heat).
 * The craft.drawPower returns a boolean: true if you succesfully drew the power, false if you did not. If you are going to do a state change it is recommended that you go about it in the following steps:
 *  1. Put your current EC draw back into the craft by craft.addPower(currentECIn);
 *  2. Cool the vessel by calling craft.coolVessel(currentHeatIn);
 * At this point you have no calculated effect on the vessel.
 *  3. Change your state. IE. Change power, change warm state, etc.
 * This should update all of your EC and Heat calculations.
 *  4. Try to draw power. If successful, emmit the heat.
 *  4a. If unsuccessful, revert state, then redraw power and heat. If this is unsucsessful you have an EC leak.
 *
 *  isActive, potentialDV, and effectiveDV are all variables specific to engines.
 *  potentialDV is the maximum DV your engine can give.
 *  effectiveDV is the current ammount of DV your engine can provide.
 *  isActive determines if this engine will be used in calculating the next jump.
 *
 *  See the warmEngine section of runTurn and the warmEngine event handler for a demonstration of doing an instant and turn bassed state-change.
 */

// TODO: 1/18/17 Impliment EC Draw and Heat Out 
public class Engine extends SubSystem {
    //Unchanging subsystem data
    protected int maxDV=45;
    protected int heatOutAtIdle=10;
    protected int heatOutWhenWarmed=25;
    protected int ecRequitedAtIdle=20;
    protected int ecRequiredPreJump=35;

    //changing variables
    protected boolean isActive=true;
    private boolean isWarmed;
    private boolean hasIdled;

    //Trackers for turnbassed state changes
    private boolean changePower;
    private boolean changeWarmStatus;




    public Engine(Vessel craft) {
        //set initial state
        super(craft);
        name="Basic Engine";
        changePower=false;
        changeWarmStatus=false;
        hasIdled=false;

    }

    /**
     * GUI Friendly toString
     * @return
     */
    public String toString(){
        String s = "Is On: "+onStatus+"\n"+
                "backend.Engine DV: "+getEffectivedV()+"\n"+
                "Current Heat Out: "+currentHeatOut()+"\n"+
                "Current EC intake: "+currentECIn()+"\n"+
                "Has Idled: "+hasIdled+"\n"+
                "Has warmed: "+isWarmed;
        return s;
    }

    /**
     * Calculates the current heat emissions of the vessel
     * @return
     */
    protected int currentHeatOut(){
        if(!onStatus)
            return 0;
        if(isWarmed)
            return heatOutWhenWarmed;
        if(onStatus)
            return heatOutAtIdle;
        else
            throw new UnsupportedOperationException("Logic Error Within Base backend.Engine Heat Logic");
    }

    /**
     * calculates the EC draw of the vessel
     * @return
     */
    protected int currentECIn(){
        if(!onStatus)
            return 0;
        if(isWarmed)
            return ecRequiredPreJump;
        if(onStatus&&!isWarmed)
            return ecRequitedAtIdle;
        else
            throw new UnsupportedOperationException("Logic Error Within Base backend.Engine EC Logic");

    }

    @Override
    /**
     * Unimplemented
     */
    public int blow(int incomingDamage) {
        return 0;
    }

    @Override
    public void runTurn() {
        if(onStatus)
            hasIdled=true;

        if(changePower){
            changePower=false;
            onStatus=true;
            if(craft.drawPower(currentECIn()))
                craft.heatVessel(currentHeatOut());
            else
                onStatus=false;
        }

        if(changeWarmStatus){ //apply turn bassed state change
            changeWarmStatus=false;

            //restore power and heat draw
            craft.addPower(currentECIn());
            craft.coolVessel(currentHeatOut());

            //change states
            isWarmed=true;

            //attempt power draw
            if(craft.drawPower(currentECIn()))
                craft.heatVessel(currentHeatOut()); //if succesful draw heat
            else { //POWER DRAW FAILED
                //revert state
                isWarmed=false;
                if(!craft.drawPower(currentECIn())){//redraw power, check for EC leak
                    System.err.println("Logical Failure in EC Acquisition for engine");
                    System.exit(5);
                }
                craft.heatVessel(currentHeatOut()); //apply original heat before state change
            }
        }
    }

    @Override
    /**
     * called when the vessel can no longer allocate power for the subsystem.
     */
    public void forceShutdown() {
        craft.addPower(this.currentECIn());
        craft.coolVessel(this.currentHeatOut());
        onStatus=false;
        isWarmed=false;
        hasIdled=false;
    }


    //Engine specific implementation from here down.
    public int getMaxDv(){
        return maxDV;
    }

    public int getEffectivedV(){
        if(isWarmed)
            return maxDV;
        else
            return 0;
    }

    public boolean isActive(){
        return isActive;
    }
}